Datenschutzerklärung zur Nutzung von Zoom

Noch gibt es keine englische Version der Datenschutzerklärung zur Nutzung von Zoom. Aber da unsere grandiosen RFC-Studis Deutsch sprechen können, werden wir nun die deutsche Datenschutzerklärung in unserem GIT verlinken.

[Datenschutzerklärung zur Nutzung von Zoom](https://video.cls.rwth-aachen.de/gebrauchsanweisungen/#rechtliche-hinweise "Seite vom CLS mit den Hinweisen")

