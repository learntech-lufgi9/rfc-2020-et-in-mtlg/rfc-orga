# Progress Report for Milestone 1
## 1. List of most important scientific/technical results (max. 2000 characters)

There were three Workpackages to reach the first Milestone. 
The first one is the familarization with the eye-tracking glasses. We only had 3 glasses available for four people. We splitted the work anyway so it was not a problem that one person was without a glasses at the beginning. The familarization was done by looking at the pupil cores documentation and further reading. Moreover the pupil core software was installed and configuration of the glasses done and first attemps were made. 
For the requirements analysis we wanted to do some interviews with possible stakeholders to get some knwoledge what is expected and which kind of research will be done with the eye tracking module.
Therefore we designed a questioannaire that was handed out during the interviews and evaluated afterwards.
The third work package was some basic literature reserach which is also completed. The literature is placed in Zotero and summeries of relevant results were prepared.

## 2. Comparison of project progress with original schedule (max. 1000 characters)

The Requirements analysis lasted a little longer because it depended to the appointments with the stakeholders. Moreover this progress comes a little later than expected due to other workload.
Ahead of the the first Milestone we already dived into some other workpackages including the software stack of pupil core, marker detection and surface tracking as well as marker integration in MTLG.
All in all everything seems to be on track.

## 3. Did the prospects of project outcome change from the original objectives and if so, why? (max. 1000 characters)

No, the original goals are most likely to be achieved on time.

## 4. Are there or will there be any neccessary changes in the objectives of the project? (max. 1000 characters)

Everything is fine.

