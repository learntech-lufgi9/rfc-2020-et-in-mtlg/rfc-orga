# Progress Report for Milestone 2
## 1. List of most important scientific/technical results (max. 2000 characters)

As designated by the project outline, the Pupil Core software stack was set up, including its April tag detection and surface tracking capabilities.
An MTLG component has been developed which can display such tags over a game running in the MTLG framework, register users and handle eye-tracking events.
The eye-tracking component is able to compute user gaze or fixation interactions with game objects or user-defined areas.
Eye-tracking data is stored in a shared database.
The user can choose to only store lightly processed raw data and do post-processing of the game session, or handle game interactions in real-time.

Additionally to the above, a representative game application was developed which tracks all users' eye movements in real-time and visualizes them.
This allows for a demonstration of the project's capabilities as they could be used by a real-world application.

## 2. Comparison of project progress with original schedule (max. 1000 characters)

Messaging, networking and event handling components turned out to be much more straightforward than anticipated.
Meanwhile, the concrete design of the eye-tracking module and its API had to work around some issues presented by the Pupil Core software and thus took longer than anticipated.
The same is true for the data storage in the database, where database handling emerged as a critical aspect to the overall performance of the whole eye-tracking pipeline.

Overall, the above leaves us a few weeks behind schedule regarding the second milestone.
However, as indicated in the description of our results, several real-time components originally designated for the third milestone have already seen significant work.
Considering this and the changes described below, we are about on time on the overall project timeline.

## 3. Did the prospects of project outcome change from the original objectives and if so, why? (max. 1000 characters)

Given the pandemic, it is still difficult to consider running user studies.
Instead, the project owners agreed to focus more on the real-time possibilities of the developed eye-tracking software stack, as well as the presentation of its capabilities.
Thus, the project outcome will definitely feature the originally facultative networking and real-time components, but will probably have to cut back on user evaluation for the time being.

## 4. Are there or will there be any necessary changes in the objectives of the project? (max. 1000 characters)

Aside from the above, the overall, scientific and technical objectives remain the same.

