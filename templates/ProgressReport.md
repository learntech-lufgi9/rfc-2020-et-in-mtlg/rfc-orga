# Progress Report
## 1. List of most important scientific/technical results (max. 2000 characters)

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam semper diam sollicitudin, facilisis orci vitae, pretium dui. Curabitur et lorem risus. Fusce sed ex nisi. Nunc imperdiet eros non massa auctor porta. Cras vulputate mollis est et mollis. Suspendisse potenti. Phasellus sodales orci non sollicitudin posuere. Aenean luctus dictum consectetur. Fusce blandit efficitur sapien ullamcorper vulputate.

Duis tempus sollicitudin dui sed finibus. Donec eu metus venenatis risus auctor viverra eget vel nisi. Donec at mi at mi dictum lobortis. Vestibulum commodo interdum urna ac accumsan. Cras interdum justo in aliquet tempus. In quam ipsum, tristique et nunc vitae, dictum commodo odio. Mauris in cursus ante. Etiam tincidunt dictum dui, a blandit mauris rutrum ac. Proin fermentum pulvinar purus, vitae bibendum lorem condimentum eu. Aliquam erat volutpat. Donec imperdiet congue lacus, at pellentesque mi imperdiet a. Maecenas quis nisi facilisis risus placerat mollis. Curabitur pellentesque justo vitae eros facilisis finibus.

Mauris eget egestas nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla rutrum at neque laoreet aliquet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin maximus justo quis faucibus ultrices. Phasellus vel ullamcorper arcu. Fusce suscipit ultrices ultrices. Integer mi risus, vehicula in mollis nec, tincidunt et elit.

Nullam finibus mauris sed sapien porttitor sodales. Aliquam erat volutpat. Nulla vitae sollicitudin mi. Cras tincidunt ante a eros rutrum, sed sodales augue consectetur. Cras sit amet metus sit amet ante dignissim sodales eu quis sem. Aenean lacinia gravida ipsum. Sed nulla est, efficitur a consequat eu, iaculis in lectus. Nunc placerat laoreet fringilla. Sed congue, libero eu laoreet laoreet, felis velit tristique quam, id ullamcorper ligula dui vitae orci. Nulla eleifend, ante in iaculis porta, mi mauris interdum tellus, id tincidunt amet.

## 2. Comparison of project progress with original schedule (max. 1000 characters)

Everything seems to be on track.

## 3. Did the prospects of project outcome change from the original objectives and if so, why? (max. 1000 characters)

No, the original goals are most likely to be achieved on time.

## 4. Are there or will there be any neccessary changes in the objectives of the project? (max. 1000 characters)

Everything is fine.

