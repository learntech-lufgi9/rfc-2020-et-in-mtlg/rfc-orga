Tutorial:
<https://try.github.io/>

Further Reading:
<https://svij.org/blog/2014/11/01/git-fur-einsteiger-teil-2/>

Cheat Sheet:
<https://gitlab.com/mtlg-framework/mtlg-gameframe/uploads/a45d6fc4cc9c6cc76b40ce09aa2b79c0/gcs-draft.pdf>

Installation:
<https://gitlab.com/mtlg-framework/mtlg-gameframe/-/wikis/help/git-referenz>

Configuration of Credentials and SSH:
<https://gitlab.com/mtlg-framework/mtlg-gameframe/-/wikis/help/git-conf>