Welcome to the Research Focus Class!

This document recommends the reading order for the project files.

First, you should start with the GroundRules.md, where we explain our intentions within the research focus class.

Second, we prepared a project outline (ProjectOutline.md), just like a project outline for one of our "real" research projects.

Third, look into the templates in the "templates" folder.

After that feel free to look around in the projects subgroub. You are currently in the orga-repository, which is kind of a meta-repository. 
Click on the sub groups name between "learntech-lufgi9" and "rfc-orga" in the bread crumb navigation elemnt above this document and take a look around.
To give you some starting point beyond the other repositories in the subgroup, the most important elements will be in the issues sub menu, namely "List", "Boards" and "Milestones".

If there are any questions, feel free to ask your supervisors!