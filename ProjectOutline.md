# Project Outline - Eye-Tracking in Multi-Touch Learning Games

## Objectives

### Theme and Problem description

Interaction Research and Learning Analytics in the context of learning games on multi-touch tabletop displays is an ongoing research focus in the Learning Technologies Research Group.
While digital interaction can be logged and tracked and there are different (semi-)automatic approaches on interaction attribution, there are still open questions with answers that might be of utter significance in collaboration research.
One past study looked into people with different personality traits playing the regular expressions game. Key indicator in this evaluation was the average recovery time after feedback. Some personality traits correlated with shorter times until the next interaction, other with much longer recovery times.
What remains unknown is the question *what* our learners do in said interval. Are they idleing, thinking for themselves? Are they looking at their team mates desperately searching for advice, clues or consolation? Are they staring at the position of their previous mistake, trying to understand it or are they already scanning the screen for the next interaction?
To be honest, not all of those questions will be answered in this research focus class, especially not the emotional kind. But still, the product of this class could deliver valuable insights into a better understanding of collaboration.

### Overall Objective

The overall objective is to introduce head-mounted eyetracking devices into an experimental multi-touch tabletop research setup. The basic idea is to introduce markers into learning games, track if the learners focus hits any area described by those markers and bring this data together with the rest of the labs data stream. 

### International State of the Art

TBD

### Scientific and Technical Objectives

Minimum requirement: 
A system where post-processing results can tell if a specified learner had his or her gaze on the screen at a given point of time or not.

Expected requirement:
A system where post-processing results can tell where a specified learner was looking at a given point in time. This can either be a screen coordinate or a semantic game element ("card", "personal area" or comparable).

Aspired requirement:
A system which offers an interface for requests from a learning game to acquire a specified learners current gaze position.

## Structure of the Network

### Network Partners

The network partners in this project consists of three main parties. 

Dipl.-Gyml. Matthias Ehlenz is a researcher in the area of multi-touch tabletop games and has experience regarding formalized projects like this. He was part of the TABULA project team and thus know a thing or two about experimental design, interaction research and the MTLG framework.

Birte Heinemann, M.Sc. is the newest member of the LuFg i9 team but isn't new to science. She already had a researchers position after her degree at the university of Paderborn and is involved in the area of eye-tracking since before her bachelor thesis. Through her work in the psychology department she also has a lot of knowledge about the design and methodology of scientific experiments.

The third party involved is the developer team, consisting of highly motivated, skilled students with above average knowledge and skills, ready to tackle a new format of university courses.

### Individual Functions in the network and planned implementation chain

Since the project is loosely constructed after agile principles, the mentioned partners can be considered to hold the following roles:

Product Owner - Matthias Ehlenz:
Interested in future usage of the product in his research framework, he communicates which features are important in this project and where the priorities are. 
He should be invited to the respective meetings (at least every milestone meeting).

Scrum Master - Birte Heinemann:
She keeps an eye on the schedule and workload and helps the group to divide tasks among themselves. She gives advice - on eyetracking, methodology or where to ask which questions.
Is present in each sprint planning - currently planned every two weeks.

The developement team is the third party and responsible for implementation, self-organization, holding deadlines and submitting deliverables.

There are five defined milestones. Each milestone concludes one phase in the research project and is accompanied with a retrospecive meeting for feedback and reflection of the work progress itself.

### Associated Partners and Application

In this case there are no partners directly associated with the project. Possible stakeholder to be taken in consideration are future researchers intending to use the developed system. 
A deeper look into possible applications is strongly recommended in WP2: Requirement Analysis.

## Description of Work Schedule

The following schedule describes the expected scenario and is based on experiences from other research projects (consider as "educated guesses").
Delays are to be expected and won't affect grading if reasons for the delays are stated in the progress reports.
All WPs have time estimates and modelled dependencies as well as defined deliverables. They are also created as issues in the meta-repository and include a definition of done (mostly consisting of defined deliverables). New issues (in sub-repos) should be linked to those issues, those issues themselves are connected to the respective milestones. The time estimation uses the unit PW, which means person-week: the amount of work performed by an average person in one week. (This or similar concepts are often used in large projects). 


![ProjectFlowChart](WorkFlowRFC2020.png "Flow Chart of the Project")

| WP1: Familiarization Eye-Tracking Glasses | 3-4 PW |
| --------- | --------|
| In Work Package 1 the project members dive into the used hardware. In this package the documentation is to be read and summaries of the most important section written up. Expected load is two team members for approx. 2 weeks each.|

| WP2: Research - Requirement Analysis | 3-4 PW |
| --------- | --------|
| Work Package 2 refines the requirements.  Brief and informal interviews with potential stakeholders as well as creation personas or use cases/user stories. Expected outcome is a filled issue tracker for upcoming packages.|

| WP3: Research & SoA - Basic Literature | 4 PW |
| --------- | --------|
| This WP consists of a thorough literature research. In this package the given literature is to be read & further sources to be researched. Expected outcome: Short summaries and brief assessments of project relevance for each source read. Expected load: One week per team member.|

| WP4: Software-Stack Pupil Core | 4 PW |
| --------- | --------|
| The given hardware (Pupil Core by Pupil Labs) is open source and so is the accompanying software stack. There are many components to be understood. Pupil Core provides an installation guide. Expected outcome of this package is a more detailed explanation of each software packages role in the stack. This could consist of flow chart and a list of brief descriptions. Expected load: Two members, two weeks each.|

| WP5: Marker Detection | 2 PW |
| --------- | --------|
| Pupil Labs' documentation states the supports of visual markers (April tags suggested). In this package, the team familiarizes with the usage of markers, conducts small, informal experiments and documents the results. Expected result: Short guideline. Expected load: Two members, one week each.|

| WP6: Marker generation in MTLG | 6 PW |
| --------- | --------|
| As the documentation states April Tags as preferred markers, this WP consists of the implementation of a MTLG component which can show specified markers at given positions. Expected load: Two members, three weeks each.|

| WP7: Marker Detection in MTLG | 4 PW |
| --------- | --------|
| This packages integrates the results of WP5 and WP6. Objective is the detection of markers in learning applications, the logging of those detections and documentation of correlating interactions with gaze data.|

| WP8: Testing | 4 PW |
| --------- | --------|
| This packages implements, in combination with it's counterpart WP9, the quality control stage. Expected outcome: Issues in the issue tracker. One week per person.|

| WP9: Bug-Fixing | 4 PW |
| --------- | --------|
| Bug-Fixing and Testing should happen in parallel following a four-eye principle. No team member should review a self written part of code. Expected outcome: Solved issues in the issue tracker. One week per person.|

Facultative Work Packages have to be considered if the project is not to far behind schedule upon arriving MS2.

| WP10: Networked Processing *facultative*| 2 PW |
| --------- | --------|
| The Pupil Core stack allows the streaming of gaze data over network. This package inplements this by making one device the master device, responsible for logging and/or communication. Expected workload: Two people, one week each.|

| WP11: Live-Evaluation *facultative* | 3-4 PW |
| --------- | --------|
| This package evaluates strategies for providing the results of the previous stages, mainly WP7, in pseudo-real time. Two people, one week for research and planning, then one week, one person for API-implementation. Person two moves on to WP12. Expected outcome: Documented API/WebSocket infrastructure.|

| WP12: MTLG Component for Live-Requests *facultative | 1 PW |
| --------- | --------|
| The client side component for WP11. One person, one week, preferrably with prior experience from WP6. Expected outcome: MTLG component with brief documentation in MTLG wiki.|

| WP13: Preparing User Studies | 4 PW |
| --------- | --------|
| This package prepares user studies to evaluate the software product. This involves preparation of the experimental setup, aquisition of participants, compiling questionaires and supervisor guidelines and declarations of consent (GDPR). Expected load: One week per person. Expected deliverable: Portfolio of artifacts.|

| WP14: Conducting User Studies | 4 PW |
| --------- | --------|
| The planned studies from WP13 are actually conducted in this package. This involves supervising according to guidelines, ensuring technical reliability and data recording. Expected load: One week per person. Expected outcome: Data.|

| WP15: Evaluation of User Studies | 4 PW |
| --------- | --------|
| The data from WP14 has to be evaluated. This evaluation is primarily for verification of the implemented gaze tracking. Explorative looks are welcome as well. Expected outcome: Statement on validity. Expected load: Two people, two weeks each.|

| WP16: Documentation of Products | 4 PW |
| --------- | --------|
| This packages reiterates over the software artifacts generated throughout the project. Here it is to be ensured that all code is documented in a similar fashion and a (condensed) developer manual is generated. Expected outcome: Developer Manual (for orientation). Expected load: One person, two weeks.|

| WP17: Utilization Strategies | 4 PW |
| --------- | --------|
| In this package, the utilization strategies are considered and prepared. This includes both a guideline for researchers on how to use the software stack and the open source release of the product. Expected outcome: a public repository containing all code and documentation. Expected load: one person, two weeks.|

| WP18: Writing of Research Paper *facultative*| 4-6 PW |
| --------- | --------|
| This packages is (individually) optional. Each participant can chose to take it or not. The results of the Research Focus Class are written up into a scientific research paper and submitted to a conference (which one is still TBA). Workload depends on results of previous work. Supervisors will be actively involved.|

## Resources and infrastructure

The Learning Technologies Innovations Lab at LuFg i9 can be used with all it's resources. This includes multi-touch tabletop displays from 27 to 84 inch screen size, four Pupil Core Eye-Tracking headsets and four powerful notebook computers. Recording equipment installed in the lab may be used as well.
The RFC team will be given the opportunity to appoint one full or two half week days (apart from wednesday) to get priority usage of the lab. 
In those reserved time slots, the lab may be booked for exclusive usage given three days notice via the supervisors requiring a brief explanatory statement.

## Utilization plan

### Scientific and technical perspectives

The final product can help researchers all over the world to understand the interaction in collaborative tasks on a much deeper level.
Future work building on top of the product could track faces, emotions, detect objects or make predictions.

### Expected learning outcome

There is a lot to be learned. Newest technology, working with complex open source software stacks, handling large data streams with python, communicating in near real-time over TCP-IP and a deeper knowledge of web technologies like JavaScript.

# Sources

A starting point for literature research is always welcome.
TBD.