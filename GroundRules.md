# The Research Focus Class

Welcome to this years Research Focus Class. The Concept of RFCs is rarely employed in the CS department, so we decided to write down some of the ground rules regarding our RFCs in this document.

# Don't be afraid

First and foremost this is our main message. Research can be frightening but shouldn't be. We did our best to support you as a "beginner" in scientific research.
This class should prepare you for upcoming research projects like your master thesis or real world research projects after your degree.
To achieve this, we decided to follow the structure of a "real" research project, based on the requirements and formats of BMBF ("Bundesministerium für Bildung und Forschung") funded projects supervised by VDI/VDE-IT or DLR (project managment agencys, service and consulting for federal and state ministries, among others). DFG (Deutsche Forschungsgemeinschaft) or EU (european funding) projects have similar requirements.

As soon as you look into the templates-folder, you will find a template for the progress report. The progress report is to be filed after every milestone. 
Apart from the first question (which is basically what you did) there are multiple questions concerning any divergence from the original plan.

These questions are there for a reason. The ultimate goal of science is to tackle the unknown. "No plan survives contact with the enemy" is a popular quote, and in this case, the unknown is your enemy.
Plans are made to be changed and we know that. Our expectation of you is that you do your best and take the class serious. And if you don't fulfill a milestones requirement in the estimated time, no one will be mad.
Write it down, state what made this task harder than expected and everything is fine. That's science.

# You are a team

Act as one. There are ups and downs, there are weeks where the workload from other classes is more demanding and there are weeks where your private life has to get priority.
Your teammates can help you, as well as you can help them. Some working packages are parallelized in the project plan, but all working packages are designed to be done by at least two people. 
You don't have to do everything together and can divide the work as you wish among the team members. You can do pair programming whenever it makes sense, but you don't have to. Whatever floats your boat ;)
Last but not least, you can consider the RFC as a team dry run for your master thesis. You can learn a lot about scientific work, writing and methodologies. And maybe even discover an interesting topic for your thesis.

# Results matter, not success, not words 

Sounds weird, but this is the heart of science. Learning. And that includes learning from mistakes.
"It does not work the way we tried it" is an important result. Not only for you, but for other researchers as well.
And those researchers can only learn from your dead-ends if you document them.
The last bit concerns the way you fix your results. More words don't make science better. Be concise. To help you learn this, we have put a character limit to the report templates. Just like in the real reports.

# Professional workflow, not surveillance

Version control is state of the art. If has reached even the last branches of German CS industry, with more or less success. To be prepared for a professional work environment, we offer you a professional environment.
To be honest, not every company has fully understood the potential of version management, but maybe you will be the person bringing your future employer to the 21st century.
And GitLab offers so much more than just version control. We want you to learn the management of projects, workflows, agile developement, issue tracking and so on. 
GitLab has the side effect of making actions traceble to the individual team member, but that is not why we are using it. We use it because it's so good and we simply love working with it. And sure hope you will, too :)